package com.example.faustdehng.myapplication

import java.util.*

class GuessNumber(){
    /**
     * answer of the game, 4 different digits, leading zero(s) is allowed
     */
    // private var answer = this.getInit()
    public var answer: String
    public var GuessDig=4

    init {
        this.answer = this.getInit()
    }


    /**
     * Get the (random) initial value for a new game
     *
     * 4 different number digits, leading zeros is allowed
     */
    private fun getInit() : String {
        var answerDigit: String
        do {
            val random = Random()
            val bound1 = Integer.parseInt("9".repeat(GuessDig))+1
            val answer = random.nextInt(bound1)
            val digits = "%d".format(answer)
            answerDigit = "0".repeat(GuessDig - digits.length)+digits
        } while(!isValid(answerDigit))
        return answerDigit
    }

    public fun gotanswer() :String {
        return this.answer
    }
    /**
     *
     */
    public fun renew(SetDig:Int) {
        GuessDig=SetDig
        this.answer=this.getInit()
    }
    /**
     * check if there are same digits
     *
     * @return true for no same digits, 4 digits are different, false for length wrong or same digits
     */
    private fun isValid(answer: String): Boolean {
        if(answer.length == GuessDig) {
            for(i in 0..GuessDig-2) {
                for(j in (i+1)..GuessDig-1) {
                    if(answer.get(i)==answer.get(j)) {
                        return false
                    }
                }
            }
            return true
        }
        return false
    }
    /**
     * Check the anser and the guess, return how many A or B
     *
     * @param   answer  the answer of the game
     * @param   guess   the new guess
     * @return  Pair of (As, Bs), or null for wrong input
     */
    public fun checkABs( guess: String): String? {
        if (this.answer.length != GuessDig || guess.length != GuessDig) {
            return null
        } else {
            var a: Int = 0
            var b: Int = 0
            for (i in 0..GuessDig-1) {
                for (j in 0..GuessDig-1) {
                    if (this.answer.get(i) == guess.get(j)) {
                        if (i == j) a += 1 else {
                            b += 1
                        }
                        break
                    }
                }
            }
            return if(a!=GuessDig) (a.toString()+"A"+b+"B") else "OK"
        }
    }
}
